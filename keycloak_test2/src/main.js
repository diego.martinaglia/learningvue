import Vue from 'vue'
import App from './App.vue'
import { createPinia, PiniaVuePlugin } from 'pinia'
import Vuetify from 'vuetify'
import Keycloak from 'keycloak-js'

import 'vuetify/dist/vuetify.min.css' 
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false

let initOptions = {
  url: process.env.VUE_APP_API_BASE_PATH, realm: 'sigrid', clientId: 'sigrid-client', onLoad: 'login-required'
}

let keycloak = new Keycloak(initOptions).on;
console.log("---keycloak = 22",keycloak)

keycloak.init({ onLoad: initOptions.onLoad }).then((auth) => {
  alert("-------")
  console.log("----init keycloak ----")
  if (!auth) {
    window.location.reload();
  } else {
    Vue.$log.info("Authenticated");

    new Vue({
      el: '#app',
      render: h => h(App, { props: { keycloak: keycloak } })
    })
  }


//Token Refresh
  setInterval(() => {
    keycloak.updateToken(70).then((refreshed) => {
      if (refreshed) {
        Vue.$log.info('Token refreshed' + refreshed);
      } else {
        Vue.$log.warn('Token not refreshed, valid for '
          + Math.round(keycloak.tokenParsed.exp + keycloak.timeSkew - new Date().getTime() / 1000) + ' seconds');
      }
    }).catch(() => {
      Vue.$log.error('Failed to refresh token');
    });
  }, 6000)

}).catch(() => {
  Vue.$log.error("Authenticated Failed");
});

const vuetify= new Vuetify({
  icons: {
    iconfont: 'md',
  },
})

Vue.use(Vuetify)
Vue.use(PiniaVuePlugin)
const pinia = createPinia()
const app = new Vue({
  render: h => h(App),
  pinia,
  vuetify : vuetify
});

app.$mount('#app');

