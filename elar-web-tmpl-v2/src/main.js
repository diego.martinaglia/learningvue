import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import Plotly from 'plotly.js-dist'
import mitt from 'mitt'
import {createPinia, PiniaVuePlugin} from "pinia/dist/pinia";
import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css' 
import '@mdi/font/css/materialdesignicons.css'


const emitter = mitt();

Vue.config.productionTip = false


Vue.use(Vuetify)
Vue.use(PiniaVuePlugin)
Vue.prototype.$axios=axios
Vue.prototype.$Plotly=Plotly
Vue.prototype.$emitter=emitter;
const pinia = createPinia()
const app = new Vue({
  render: h => h(App),
  pinia,
  vuetify : new Vuetify()
});

app.$mount('#app');
console.log("-----here we go---process.env=",process.env);
