function createColorsWithHSV(total) {       
    console.log("----total=",total);
    var i = 360 / (total - 1); // distribute the colors evenly on the hue range   
    console.log("----i=",i);
    var r = []; // hold the generated colors
    for (var x = 0; x < total; x = x + 1) {
      console.log("------i * x =", i * x);
      r.push(hsv2rgb(i * x, 0.7, 1)); // you can also alternate the saturation and value for even more contrast between the colors
    }      
    return r;
  }
 function  HSVtoRGB(h, s, v) {
    var r, g, b, i, f, p, q, t;
    if (arguments.length === 1) {
      s = h.s, v = h.v, h = h.h;
    }
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
      case 0: r = v, g = t, b = p; break;
      case 1: r = q, g = v, b = p; break;
      case 2: r = p, g = v, b = t; break;
      case 3: r = p, g = q, b = v; break;
      case 4: r = t, g = p, b = v; break;
      case 5: r = v, g = p, b = q; break;
    }
    return {
      r: Math.round(r * 255),
      g: Math.round(g * 255),
      b: Math.round(b * 255)
    };
  }

  function hsv2rgb(h,s,v)  {                              
  let f= (n,k=(n+h/60)%6) => v - v*s*Math.max( Math.min(k,4-k,1), 0);  
  let resuls = {r:1,g:2};   
  return {r:Math.round(f(5)*255),    g: Math.round(f(3)*255),    b:Math.round(f(1)*255)};       
}   


  let listColor = createColorsWithHSV(28);
  console.log("-----listColor=",listColor);


