import {useGlobalStore} from '../stores/globalStore.js';
import {colorStoreFactory} from '../stores/colorStoreFactory.js'
export const statMixin = {
  setup() {  
    let globalStore = useGlobalStore();
    return  {globalStore,colorStoreFactory}
  },
  props: {   
    A42:Number,
    compParam:{
      type:Object
    }
  },
  data() {
    return {
      storePerService:null,
      resultDict: {},
      fromDate: this.convert2LocalDate(this.dateMinusYears(1)),
      toDate: this.convert2LocalDate(new Date()),     
      traceList: [],
      layout:{},
      sliderChangeTimer: null     
    };
  },
  computed: {
    dict_visibility:function () {
      if (this.storePerService == null) {
        return {};
      }
      return this.storePerService.dict_visibility
    },
    nbrTraces: function() {
      return Object.keys(this.resultDict).length
    },
    divId: function() {
      console.log("---------this.compParam=",this.compParam);
      return this.compParam.url_request_path + "-" +  this.A42;
    }
  },
  watch:{
    dict_visibility: function() {
      this.modify_visibility();    
    }
  },
  methods: {
    async getData(fromDate, toDate) {   
      let urlParams = { params: {} };
      if (fromDate != null && toDate != null) {
        urlParams = {
          params: {
            fromDate: fromDate,
            toDate: toDate
          }
        };
      }
      urlParams.params.A42 = this.A42;
      console.log(">>>>>>>>>>>>>>>>>>>>>>>> getData for :", this.divId);
      try {
        const response = await this.$axios.get(
          process.env.VUE_APP_REST_ENDPOINT + "/" + this.compParam.url_request_path,
          urlParams
        );
        console.log("<<<<<<<<<<<<<<<<<<<<<< data received for :",this.divId)
        for (let item of response.data) {
          if (!(item[this.compParam.column4trace_param] in this.resultDict)) {
            this.resultDict[item[this.compParam.column4trace_param]] = [];
          }
          this.resultDict[item[this.compParam.column4trace_param]].push(item);
        }
      } catch (error) {
        console.error(error);
      }
    },
    createAllTraces(xKey, yKey) {         
      for (let key of Object.keys(this.resultDict)) {
        let trace = this.createTrace(key);
        let average = 0;
        for (let data of this.resultDict[key]) {
          trace.x.push(data[xKey]);
          trace.y.push(data[yKey]);
          average += data[yKey];
        }
        average = average / trace.x.length;
        trace.average = average;
        this.traceList.push(trace);       
      }

      this.traceList.sort(function (trace1, trace2) {
        return trace2.average - trace1.average;
      });
    },
    setColors2Traces() {
      for (let count = 0; count < this.traceList.length;count ++) {
        let trace = this.traceList[count];
        if (trace.type == "line") {
          trace.line={color:this.storePerService.colorPerTrace[trace.name]};
        } else if (trace.type=="bar") {
          trace.marker={color:this.storePerService.colorPerTrace[trace.name]}
        }
      }
    },
    createTrace(key) {          
      let trace = {
          name: this.resultDict[key][0][this.compParam.column4trace_title],
          data_key: this.resultDict[key][0][this.compParam.column4trace_param],
          x: [],
          y: [],
          type: this.compParam.plotly_type        
        };    
      return trace;
    },
    modify_visibility() {      
      for (let index = 0; index < this.traceList.length; index++) {
        let trace = this.traceList[index];
        if (trace.name in this.dict_visibility) {
          trace.visible = this.dict_visibility[trace.name];
        }
      }  
      this.$Plotly.react(this.divId,this.traceList,this.layout);      
    },
    convert2LocalDate(date) {
      let month = date.getMonth() + 1;
      let day = date.getDate();
      let dateString = day + "." + month + "." + date.getFullYear();
      // console.log("----in the converter:",date,",dateString=", dateString);  
      return dateString;
    },
    dateMinusYears(years) {
      var date = new Date();
      date.setFullYear(date.getFullYear() - years);
      return date;
    }
  },
};
