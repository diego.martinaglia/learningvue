import { defineStore } from 'pinia'

const colorStoreFactory = function(name)  {
return defineStore(name, {
    state: () => ({
        name: name ,
        listColor:[],
        colorPerTrace: {},  
        listActualMainTraces:[],
        dict_visibility:{},
        loading: false,
        error: null
      })  
  })
}

export  {colorStoreFactory};
  