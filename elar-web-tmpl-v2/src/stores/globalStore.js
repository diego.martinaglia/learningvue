import { defineStore } from "pinia";

export const useGlobalStore = defineStore("globalStore", {
  state: () => ({ 
    name:"globalStore",
    selectedService:null,   
  })
});
