import Vue from 'vue'
import axios from 'axios'
import Plotly from 'plotly.js-dist'
import {createPinia, PiniaVuePlugin} from "pinia";
import Vuetify from 'vuetify'
import VueLogger from 'vuejs-logger'

import HomeComp from './components/HomeComp.vue'
import TestButton from './components/TestButton.vue'
import NotFound from './components/NotFound.vue'

const isProduction=process.env.NODE_ENV=='production'


const routes = {
  '/': HomeComp,
  '/testbutton': TestButton
}

const VueLoggerOptions={
  isEnables:true,
  logLevel:isProduction ? 'error' :'debug',
  stringifyArguments:false,
  showLogLevel:true,
  showMethodName:true,
  separator:'|',
  showConsoleColors:true
};

Vue.use(VueLogger,VueLoggerOptions)



import 'vuetify/dist/vuetify.min.css' 
import '@mdi/font/css/materialdesignicons.css'




Vue.config.productionTip = false


Vue.use(Vuetify)
Vue.use(PiniaVuePlugin)
Vue.prototype.$axios=axios
Vue.prototype.$Plotly=Plotly

const app = new Vue({
  data: {
    currentRoute: window.location.pathname
  },
  computed: {
    ViewComponent () {
      return routes[this.currentRoute] || NotFound
    }
  },
  render (h) { return h(this.ViewComponent) },
  pinia:createPinia(),
  vuetify : new Vuetify()
});

app.$mount('#app');
