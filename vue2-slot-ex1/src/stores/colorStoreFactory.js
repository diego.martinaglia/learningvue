import { defineStore } from 'pinia'

const colorStoreFactory = function(name)  {
return defineStore({
    id:name,
    state: () => ({
        name: name ,
        listColor:[],
        colorPerTrace: {},  
        colorPerTraceChange:false,
        listTraceNamesMainComponent:[],
        dict_visibility:{},
        fromDate: convert2LocalDate(dateMinusYears(3)),
        toDate: convert2LocalDate(new Date()),             
        loading: false,
        error: null
      })  
  })
}

function convert2LocalDate(date) {
  let month = date.getMonth() + 1;
  let day = date.getDate();
  let dateString = day + "." + month + "." + date.getFullYear();
  return dateString;
}

function dateMinusYears(years) {
  var date = new Date();
  date.setFullYear(date.getFullYear() - years);
  return date;
}

export  {colorStoreFactory};
  