import { createRouter, createWebHistory } from 'vue-router'
import PostsView from './views/PostsView.vue'

const router = createRouter({
  history: createWebHistory(), 
  routes: [
    {
      path: '/',
      name: 'posts',
      component: PostsView
    },
    {
        path: '/authors',
        name: 'authors',
        // route level code-splitting
        // this generates a separate chunk (About.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import('./views/AuthorsView.vue')
    },
  ]
})

export default router