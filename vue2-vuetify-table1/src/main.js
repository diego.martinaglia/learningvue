import Vue from 'vue'
import App from './App.vue'
import { createPinia, PiniaVuePlugin } from 'pinia'
import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css' 
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false


const vuetify= new Vuetify({
  icons: {
    iconfont: 'md',
  },
})

Vue.use(Vuetify)
Vue.use(PiniaVuePlugin)
const pinia = createPinia()
const app = new Vue({
  render: h => h(App),
  pinia,
  vuetify : vuetify
});

app.$mount('#app');
