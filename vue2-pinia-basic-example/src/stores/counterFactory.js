import { defineStore } from 'pinia'

const counterFactory = function(name,init)  {
return defineStore(name, {
    state: () => ({ count: init, name: name ,locked:false}),
    getters: {
      doubleCount: (state) => state.count * 2,
    },
    actions: {
      increment() {
        this.count++
      },
    },
  })
}

export  {counterFactory};
  