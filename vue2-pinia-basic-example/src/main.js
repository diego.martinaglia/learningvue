import Vue from 'vue'
import App from './App.vue'
import { createPinia, PiniaVuePlugin } from 'pinia'
import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css' 
import 'vue-material-design-icons/styles.css'



Vue.config.productionTip = false


Vue.use(Vuetify)
Vue.use(PiniaVuePlugin)

const pinia = createPinia()
const app = new Vue({
  render: h => h(App),
  pinia,
  vuetify : new Vuetify()
});

app.$mount('#app');
console.log("-----here we go---process.env=",process.env);