import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import HelloWorld from './components/HelloWorld.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/english', component: HelloWorld , props:{msg:"Good Morning"}},
  { path: '/french', component: HelloWorld,  props:{msg:"Bonjour"} }
]

const router = new VueRouter({
  routes
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')