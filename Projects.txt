

3. dynamic-vue-comp
Sources:
https://www.koderhq.com/tutorial/vue/component-dynamic/

4. vue-pinia-basic-example

Adapted to use a factory to create stores.
Sources:
https://pinia.vuejs.org/core-concepts/
https://pinia.vuejs.org/cookbook/options-api.html
https://medium.com/geekculture/emergency-pinia-course-7a80b8ed0b04



5. vue-pinia-ex1
Using Pinia
Sources:
https://blog.logrocket.com/complex-vue-3-state-management-pinia/



Technologies:

- vue3
- Composition API
- pinia


6. vue2-vuetify-table1

Sources: 
https://vuetifyjs.com/en/getting-started/installation/#vue-ui-install
https://vuetifyjs.com/en/components/data-tables/#multi-sort
https://www.bezkoder.com/vuetify-data-table-example/

To show the icon with the correct CSS:
https://vuetifyjs.com/en/features/icon-fonts/#install-material-icons

To define some layouts:
https://vuetifyjs.com/en/features/layouts/#dynamic-layouts

To style individual rows:
https://stackoverflow.com/questions/50136503/styling-individual-rows-in-a-vuetify-data-table

Add a search field:
https://dev.vuetifyjs.com/en/components/data-tables/#custom-filter

item-style not available yet:
https://github.com/vuetifyjs/vuetify/issues/15049

Use v-lot:item to define a style per row:
https://stackoverflow.com/questions/63826446/how-to-set-the-background-color-of-vuetify-v-data-table-row-on-hover
https://codesandbox.io/s/epic-gareth-84qp1?file=/src/components/Playground.vue:89-387


7. Vue Slots:

Sources:
https://learnvue.co/tutorials/vue-slots-guide

Example of slots in v-data-table:
https://codesandbox.io/s/epic-gareth-84qp1?file=/src/components/Playground.vue:89-387



8. vue2-vuetify-table2

Source:
https://stackoverflow.com/questions/57671044/select-all-rows-of-a-vuetify-data-table-with-custom-table-body-implementation



9. vue2-pinia-with-param
Sources:
https://pinia.vuejs.org/core-concepts/

https://github.com/vuejs/pinia/issues/557

https://github.com/vuejs/pinia/discussions/784


10. Vue with route and keycloak example
Sources:
https://www.keycloak.org/securing-apps/vue
https://medium.com/codingthesmartway-com-blog/vue-js-2-quickstart-tutorial-2017-246195cfbdd2
